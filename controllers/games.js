const Game = require("../models/Game");

exports.route = (req, res) => { res.json({test: "test answer"}) };

exports.getAll = async (req, res) => {
    let allGames = {};
    allGames = await Game.find({})
        .sort([["Name", "desc"]])
        .exec();

    res.json(allGames);
}

exports.getOne = async (req, res) => {
    try {
        const game = await Game.findById(req.params._id).exec();
        res.json(game);
    } catch (error) {
        res.status(400).send(`No game with id ${req.params._id} was found`);
    }
}

exports.createOne = async (req, res) => {
    try {
        const newGame = await new Game({...req.body}).save();
        res.json(newGame);
    } catch (error) {
        res.status(400).send(`Could not create new game`);
    }
}

exports.editOne = async (req, res) => {
    try {
        const editedGame = await Game.findOneAndUpdate({_id: req.params._id}, req.body, {new: true}).exec();
        res.json(editedGame);
    } catch (error) {
        res.status(400).send(`Error updating game with id ${req.params._id}`);
    }
}

exports.deleteOne = async (req, res) => {
    try {
        const deletedGame = await Game.findOneAndDelete({_id: req.params._id}).exec();
        res.json(deletedGame);
    } catch (error) {
        res.status(400).send(`Could not delete game with id ${req.params._id}`);
    }
}