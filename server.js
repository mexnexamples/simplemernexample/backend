const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const gamesRoutes = require("./routes/games");
require("dotenv").config();

const app = express();

app.use(cors());
app.use(express.json());
app.use("/api", gamesRoutes);

mongoose.connect(process.env.MONGODBURL, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: true,
    useUnifiedTopology: true
})
.then(() => {
    console.log("DB connected");
})
.catch((e) => { console.log(`DB connection error: ${e}`) })

const port = process.env.PORT || 8000;
app.listen(port, () => { console.log(`I am listening... on port ${port}`) });