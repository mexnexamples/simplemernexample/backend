const express = require("express");
const router = express.Router();

const { 
    getAll,
    getOne,
    createOne,
    editOne,
    deleteOne,
    route 
} = require("../controllers/games");

router.get("/route/", route)

router.get("/games/all", getAll);
router.get("/game/:_id", getOne);
router.post("/game/", createOne);
router.put("/game/:_id", editOne);
router.delete("/game/:_id", deleteOne);


module.exports = router;