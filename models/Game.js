const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const constants = require("../constants");

// { _id, name, brand, price, replayability, genre, bloodAndGore }
const gameSchema = new Schema({
    Name: {
        type: String,
        unique: true
    },
    Brand: String,
    Price: Number,
    Replayability: {
        type: Number,
        min: 0,
        max: 100
    },
    Genre: {
        type: String,
        enum: constants.gameGenres
    },
    BloodAndGore: Boolean
});

module.exports = mongoose.model("Game", gameSchema);